const arrayUtils = require('./src/utils/array.utils');

const values = arrayUtils.distinctPropertyValues([{ var: `test` }, 3, {}, { var: `test2`, foo: `bar` }, { var: `test`, foo: `bar` }, { bar: `foo` }], `var`);
console.log(values);