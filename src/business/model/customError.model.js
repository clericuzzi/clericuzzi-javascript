module.exports = class CustomError extends Error
{
    /**
     * creates an instance of an error and throws it
     *
     * @static
     * @param {string} message the original error message
     * @param {string} humanMessage the message to be displayed at the ui level
     * @param {number} code the error's code
     * @param {string} devMessage the developer leven information
     */
    static new(message, humanMessage, code, devMessage)
    {
        throw new CustomError(message, humanMessage, code, devMessage);
    }

    /**
     *Creates an instance of CustomError.
     * @param {string} message the original error message
     * @param {string} humanMessage the message to be displayed at the ui level
     * @param {number} code the error's code
     * @param {string} devMessage the developer leven information 
     */
    constructor(message, humanMessage, code, devMessage)
    {
        super(message)
        this.code = code;
        this.devMessage = devMessage;
        this.humanMessage = humanMessage;
    }
};