const fs = require('fs');
const typeUtils = require('../../utils/type.utils');
const fileManager = require('./fileSystem.manager');

/**
 * reads a file and writes it as a bse64 string
 *
 * @param {*} path the file's path
 * @returns {string} a base64 encoded string representing the file
 */
module.exports.readToBase64 = function (path)
{
    if (!this.exists(path))
        throw new Error(`TextFileManager readToBase64: the file '${path}' does not exist`);

    return Buffer.from(fs.readFileSync(path)).toString('base64');
};

/**
 * checks if a file or folder exists
 *
 * @param {string} path the file's (or folder's) path
 * @returns {boolean} TRUE if the path exists in the file system, false otherwise
 */
module.exports.exists = function (path)
{
    return fs.existsSync(path);
};

/**
 * deletes a file if it exists
 *
 * @param {string} path the path to the file
 */
module.exports.deleteIfExists = function (path)
{
    if (fs.existsSync(path))
        fs.unlinkSync(path);
};

/**
 * creates a file if it does not already axists
 *
 * @param {string} path the path to the file
 */
module.exports.createIfNotExists = function (path)
{
    if (!fs.existsSync(path))
        fs.writeFileSync(path, ``);
};

/**
 * writes the given content to a file
 *
 * @param {string} path the path to the text file
 * @param {string} content the content to be written
 */
module.exports.write = function (path, content, overwrite = true) 
{
    if (overwrite)
    {
        this.deleteIfExists(path);
        this.createIfNotExists(path);
    }

    const raw = fs.readFileSync(path).toString();
    if (raw.length > 0)
        fs.appendFileSync(path, `\r\n${content}`);
    else
        fs.appendFileSync(path, `${content}`);
};

/**
 * appends a line to a existing text document
 *
 * @param {string} path the path to the text file
 * @param {string} line the content to be appended
 */
module.exports.appendLine = function (path, line) 
{
    this.createIfNotExists(path);
    const raw = fs.readFileSync(path).toString();
    if (raw.length > 0)
        fs.appendFileSync(path, `\r\n${line}`);
    else
        fs.appendFileSync(path, `${line}`);
};

/**
 * reads a text file and returns it's contend in an array of strings
 *
 * @param {*} path the file to be read
 * @returns {[string]} a string array containing all the lines in the file
 */
module.exports.readToArraySync = function (path)
{
    if (!fileManager.exists(path))
        throw new Error(`TextFileManager readToTextSync: the file '${path}' does not exist`);

    const raw = fs.readFileSync(path);
    if (typeUtils.isValid(raw) && raw.length > 0)
    {
        const data = raw.toString().split("\n");
        for (let i = 0; i < data.length; i++)
        {
            const lineLength = data[i].length;
            if (data[i].endsWith(`\r`))
                data[i] = data[i].substring(0, lineLength - 1);
            if (data[i].endsWith(`\r\n`))
                data[i] = data[i].substring(0, lineLength - 2);
            if (data[i].endsWith(`\n`))
                data[i] = data[i].substring(0, lineLength - 1);
        }

        return data.filter(i => i.trim().length > 0);
    }
    else
        return [];
};

/**
 * reads a text file and returns a string
 *
 * @param {*} path the file to be read
 * @returns {string} a string containing the whole text in the file
 */
module.exports.readToTextSync = function (path)
{
    if (!fileManager.exists(path))
        throw new Error(`TextFileManager readToTextSync: the file '${path}' does not exist`);

    const lines = this.readToArraySync(path);
    return lines.join(`\n`);
};