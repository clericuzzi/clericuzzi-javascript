const fs = require('fs');
const path = require('path');

/**
 * gets the current executing folder
 *
 * @returns the current executing folder
 */
module.exports.currentPath = function ()
{
    return process.cwd();
};

/**
 * gets the full path for a file located in the executing folder
 * 
 * @param {string} file the file's name
 * @returns the full path to the given file (based on the current executing foldrr)
 */
module.exports.currentPathFile = function (file)
{
    return path.join(process.cwd(), file);
};

/**
 * lists all the files in a given folder path
 *
 * @param {string} folder the path to have it's files listed
 * @param {boolean} recursive if TRUE the function will go through all subfilders
 * @returns {[string]} a list of files found within the given folder
 */
module.exports.folderGetFiles = function (folder, recursive)
{
    if (folder)
        folder = folder.trim();
    if (!folder)
        throw new Error(`FileSystemManager folderGetFiles: you must provide a folder path`);

    let parsed = path.parse(folder);
    if (!parsed.root || !parsed.dir)
        throw new Error(`FileSystemManager folderGetFiles: invalid folder path`);

    let dirFiles = [];
    const dirItems = [];
    const files = fs.readdirSync(folder);
    for (let file of files)
        dirItems.push(path.join(folder, file));

    for (let item of dirItems)
    {
        if (this.exists(item))
        {
            if (!fs.lstatSync(item).isDirectory())
                dirFiles.push(item);
            else
            {
                if (recursive)
                    dirFiles = dirFiles.concat(this.folderGetFiles(item, recursive));
            }
        }
    }

    return dirFiles;
}

/**
 * moves all the files folder's files to a new one
 *
 * @param {string} folder the folder to have it's files transfered
 * @param {string} newFolder the files's destination
 * @param {boolean} recursive if TRUE the function will go throught all subfolders
 * @param {boolean} overwrite it TRUE, and any of the destination files already exists they'll be orverwritten
 */
module.exports.folderCopyFiles = function (folder, newFolder, recursive, overwrite)
{
    let files = this.folderGetFiles(folder, recursive);
    for (let file of files)
    {
        const parsedFile = path.parse(file);
        const newPath = path.join(newFolder, parsedFile.base);
        this.fileCopy(file, newPath, overwrite);
    }
}
/**
 * moves all the files folder's files to a new one
 *
 * @param {string} folder the folder to have it's files transfered
 * @param {string} newFolder the files's destination
 * @param {boolean} recursive if TRUE the function will go throught all subfolders
 * @param {boolean} overwrite it TRUE, and any of the destination files already exists they'll be orverwritten
 */
module.exports.folderMoveFiles = function (folder, newFolder, recursive, overwrite)
{
    let files = this.folderGetFiles(folder, recursive);
    for (let file of files)
    {
        const parsedFile = path.parse(file);
        const newPath = path.join(newFolder, parsedFile.base);
        this.fileMove(file, newPath, overwrite);
    }
}

/**
 * deletes all the files in a given folder
 *
 * @param {string} folder the folder to have its files deleted
 * @param {boolean} recursive if TRUE the function will go throught all subfolders
 */
module.exports.folderDeleteFiles = function (folder, recursive)
{
    let files = this.folderGetFiles(folder, recursive);
    for (let file of files)
        fs.unlinkSync(file);
}
/**
 * lists all directories contained in a given folder
 *
 * @param {string} folder the folder to be analysed
 * @param {boolean} recursive if TRUE the function will go throught all subfolders
 * @returns a list of folders inside the given one
 */
module.exports.folderGetDirectories = function (folder, recursive)
{
    if (folder)
        folder = folder.trim();
    if (!folder)
        throw new Error(`FileSystemManager folderGetDirectories: you must provide a folder path`);

    let parsed = path.parse(folder);
    if (!parsed.root || !parsed.dir)
        throw new Error(`FileSystemManager folderGetDirectories: invalid folder path`);

    let dirFiles = [];
    const dirItems = [];
    const files = fs.readdirSync(folder);
    for (let file of files)
        dirItems.push(path.join(folder, file));

    for (let item of dirItems)
    {
        if (this.exists(item))
        {
            if (fs.lstatSync(item).isDirectory())
            {
                if (recursive)
                    dirFiles = dirFiles.concat(this.folderGetDirectories(item, recursive));
                dirFiles.push(item);
            }
        }
    }

    return dirFiles;
}
/**
 * creates a folder if it doesn't already exists
 * if any of the folders in the path does no exits, it will be created
 * 
 * @static
 * @param {string} filepath the full path to the folder
 */
module.exports.folderCreateIfNotExists = function (folder)
{
    if (folder)
        folder = folder.trim();
    if (!folder)
        throw new Error(`FileSystemManager folderCreateIfNotExists: you must provide a folder path`);

    let parsed = path.parse(folder);
    if (!parsed.root || !parsed.dir)
        throw new Error(`FileSystemManager folderCreateIfNotExists: invalid folder path`);

    let subfolders = [folder];
    while (parsed.root !== parsed.dir)
    {
        subfolders.push(parsed.dir);
        parsed = path.parse(parsed.dir);
    }
    subfolders = subfolders.reverse();
    for (let subfolder of subfolders)
        if (!fs.existsSync(subfolder))
            fs.mkdirSync(subfolder);
}

module.exports.fileCopy = function (file, newPath, overwrite)
{
    if (!this.exists(file))
        throw new Error(`FileSystemManager moveFile: the file '${file}' does not exist`);
    if (this.exists(newPath) && !overwrite)
        throw new Error(`FileSystemManager moveFile: the file '${newPath}' already exist`);

    fs.copyFileSync(file, newPath);
}
module.exports.fileMove = function (file, newPath, overwrite)
{
    if (!this.exists(file))
        throw new Error(`FileSystemManager moveFile: the file '${file}' does not exist`);
    if (this.exists(newPath) && !overwrite)
        throw new Error(`FileSystemManager moveFile: the file '${newPath}' already exist`);

    fs.renameSync(file, newPath);
}

/**
 * creates a file if it doesn't already exists
 * 
 * @static
 * @param {string} filepath the full path to the file
 */
module.exports.fileCreateIfNotExists = function (filepath)
{
    if (filepath)
        filepath = filepath.trim();
    if (!filepath)
        throw new Error(`FileSystemManager fileCreateIfNotExists: you must provide a file path`);

    const folderpath = path.dirname(filepath);
    this.folderCreateIfNotExists(folderpath);

    if (!fs.existsSync(filepath))
        fs.writeFileSync(filepath, '');
}

/**
 * tests if a path (folder of file) exists in the file system
 *
 * @static
 * @param {string} itemPath the path to be tested
 * @returns TRUE if the path exists
 */
module.exports.exists = function (itemPath)
{
    if (itemPath)
        itemPath = itemPath.trim();
    if (!itemPath)
        throw new Error(`FileSystemManager exists: you must provide a path`);

    let parsed = path.parse(itemPath);
    if (!parsed.root || !parsed.dir)
        throw new Error(`FileSystemManager exists: invalid path`);

    return fs.existsSync(itemPath);
};
/**
 * deletes (folder of file) exists in the file system
 *
 * @static
 * @param {string} path the path to be deleted
 */
module.exports.delete = function (itemPath)
{
    if (itemPath)
        itemPath = itemPath.trim();
    if (!itemPath)
        throw new Error(`FileSystemManager delete: you must provide a path`);

    let parsed = path.parse(itemPath);
    if (!parsed.root || !parsed.dir)
        throw new Error(`FileSystemManager delete: invalid path`);

    if (fs.existsSync(itemPath))
        fs.unlinkSync(itemPath);
};

/**
 * reads all files within a folder and delete the ones with the given extension
 * 
 * @param {string} folder the folder to be affected
 * @param {boolean} recursive if TRUE the function will go through all subfilders
 * @param {Array<string>} extensions a list of extensions that, if we find any file, will be deleted
 */
module.exports.deleteByExtention = function (folder, recursive, ...extensions)
{
    const files = this.folderGetFiles(folder, recursive);
    for (let extension of extensions)
    {
        const groupFiles = files.filter(i => i.endsWith(extension));
        for (let groupFile of groupFiles)
            this.delete(groupFile);
    }
};

/**
 * returns the folder's content as a base64 array
 *
 * @param {string} folder the folder that contains the files
 */
module.exports.folderToBase64 = function (folder)
{
    if (!this.exists(folder))
        throw new Error(`The folder '${folder}' does not exist`);

    else
    {
        const files = this.folderGetFiles(folder, true);
        const filesBase64 = [];
        for (let file of files)
        {
            const document = fs.readFileSync(file);
            filesBase64.push(new Buffer(document).toString(`base64`));
        }

        return filesBase64;
    }
};
/**
 * saves a base64 file to te disk
 *
 * @param {string} filePath the file location
 * @param {string} data the base64 data to be stored
 * @returns TRUE if everything went fine, false otherwise
 */
module.exports.decodeBase64File = function (filePath, data)
{
    try
    {
        if (this.exists(filePath))
            this.delete(filePath);

        fs.writeFileSync(filePath, new Buffer(data, 'base64'));

        return true;
    }
    catch (err)
    {
        console.log(`DecodeBase64File:`, err);
        return false;
    }
};