module.exports = {
    Date: 'date',
    Time: 'time',
    String: 'string',
    Boolean: `boolean`,
    Numeric: 'numeric',
    Datetime: 'datetime',
    Currency: 'currency',
}