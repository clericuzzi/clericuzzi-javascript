const types = {
    null: `null`,
    number: `number`,
    string: `string`,
    boolean: `boolean`,
    undefined: `undefined`,

    array: `array`,
    object: `object`,

    set: `set`,
    math: `Math`,
    error: `error`,
    symbol: `symbol`,
    regExp: `regExp`,
    function: `function`,
};
Object.freeze(types);
module.exports.Types = () => types;

/** validates a given object testing if it is a set */
module.exports.isSet = function (value) { return typeof value === types.set; }
/** validates a given object testing if it is an error */
module.exports.isError = function (value) { return typeof value === types.error; }
/** validates a given object testing if it is a number */
module.exports.isNumber = function (value) { return typeof value === types.number; }
/** validates a given object testing if it is a regular expression */
module.exports.isRegExp = function (value) { return typeof value === types.regExp; }
/** validates a given object testing if it is an object */
module.exports.isObject = function (value) { return typeof value === types.object; }
/** validates a given object testing if it is a boolean */
module.exports.isBoolean = function (value) { return typeof value === types.boolean; }
/** validates a given object testing if it is a function */
module.exports.isFunction = function (value) { return typeof value === types.function; }

/** validates a given object testing if it is a string */
module.exports.isString = function (value) { return typeof value === types.string; }
/** validates a given object testing if it is a string with length greater than 0 */
module.exports.isStringAndValid = function (value) { return typeof value === types.string && value.length > 0; }

/** validates a given object testing if it is an array */
module.exports.isArray = function (value) { return Array.isArray(value); }
/** validates a given object testing if it is an array */
module.exports.isArrayAndValid = function (value) { return Array.isArray(value) && value.length > 0; }

/** validates a given object testing if it is an empty constructor */
module.exports.isEmpty = function (value) { return Object.entries(value).length === 0 && value.constructor === Object; }

/** validates a given object testing if it is valid for json stringfication */
module.exports.isJsonValid = function (value)
{
    if ((value === null || value === undefined)
        || this.isEmpty(value)
        || (this.isArray(value) && value.length === 0))
        return false;

    return true;
}

/** validates a given object testing if it is not null nor undefined */
module.exports.isValid = function (value)
{
    if (this.isNumber(value))
        return value !== null && value !== undefined && !isNaN(value);
    if (this.isString(value))
        return value !== null && value !== undefined && value !== ``;
    else
        return value !== null && value !== undefined && !this.isEmpty(value);
}