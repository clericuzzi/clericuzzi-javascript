const moment = require('moment');
const typeUtils = require('./type.utils');
const arrayUtils = require('./array.utils');

module.exports.dateFormat = `DD/MM/YYYY`;
module.exports.dateInputFormat = `DDMMYYYY`;

module.exports.parse = function (value, format = this.dateInputFormat) 
{
    const dateValue = moment(value, format);
    if (dateValue.isValid())
        return dateValue.toDate();
    else
        return null;
}
module.exports.toHour = function (value) { return moment(value).format(`HH:mm`); }
module.exports.toTime = function (value) { return moment(value).format(`HH:mm:ss`); }
module.exports.isValid = function (value, format = this.dateInputFormat) { return this.parse(value, format) !== null; }
module.exports.toDateBr = function (value) { return moment(value).format(this.dateFormat); }
module.exports.toDateTimeBr = function (value) { return moment(value).format(`DD/MM/YYYY HH:mm`); }
module.exports.toRelevantTimeBr = function (value) 
{
    const today = moment(new Date());
    const yesterday = moment(new Date()).add(-1, `day`);
    const dateValue = moment(value);
    if (dateValue.isSame(today, `day`))
        return this.toHour(value);
    else if (dateValue.isSame(yesterday, `day`))
        return `ontem`;
    else
        return dateValue.format(`DD/MM/YYYY`);
}

module.exports.today = function () { return moment().toDate(); }
module.exports.firstOfMonth = function () { return moment().date(1).toDate(); }

module.exports.todayFormatted = function () { return moment().format(this.dateFormat); }
module.exports.firstOfMonthFormatted = function () { return moment().date(1).format(this.dateFormat); }

module.exports.dateBrToServer = function (value) { return moment(value, this.dateFormat).format(`YYYY-MM-DD`); }

module.exports.dateTo = function (value, format = this.dateFormat) 
{
    try
    {
        return moment(value, format).format(`YYYY-MM-DD 23:59:59.999`)
    }
    catch (err)
    {
        return null;
    }
}
module.exports.dateFrom = function (value, format = this.dateFormat) 
{
    try
    {
        return moment(value, format).format(`YYYY-MM-DD 00:00:00.000`)
    }
    catch (err)
    {
        return null;
    }
}

/**
 * returns a distinct list of dates extracted from a property inside the given array objects
 *
 * @param {array} dataArray the array that contains the data
 * @param {string} property the property to extract the dates from
 * @param {array} notAllowedDays (OPTIONAL) the output format
 * @returns array - the distinct dates contained in the input array
 */
module.exports.distinctDates = function (dataArray, property, format = `YYYY-MM-DD`) 
{
    try
    {
        if (!typeUtils.isArrayAndValid(dataArray))
            throw new Error(`a valid object array must be passed in order to get distinct dates`);
        if (!typeUtils.isStringAndValid(property))
            throw new Error(`a valid property must be passed in order to get distinct dates`);

        const dates = [...new Set(dataArray.map(i => moment(i[property]).format(`YYYY-MM-DD`)))];
        return arrayUtils.sortAsc(dates);
    }
    catch (err)
    {
        throw err;
    }
}


/**
 * returns a list of dates inside the given interval
 *
 * @param {date} initialDate the initial date in the interval
 * @param {date} finalDate the final date in the interval
 * @param {string} format (OPTIONAL) the output format
 * @returns array - the distinct dates contained in the input array
 */
module.exports.datesInInterval = function (initialDate, finalDate, format = `YYYY-MM-DD`) 
{
    try
    {
        if (!initialDate)
            throw new Error(`you must define an initial date`);
        if (!finalDate)
            throw new Error(`you must define a final date`);

        let dates = [];
        let endDate = moment(finalDate).set({ hour: 23, minute: 59, second: 59, millisecond: 999 });
        let initDate = moment(initialDate).set({ hour: 0, minute: 0, second: 0, millisecond: 0 });

        if (endDate < initDate)
            throw new Error(`the initial date cannot be after the final one`);

        while (initDate < endDate)
        {
            dates.push(initDate.format(format));
            initDate = initDate.add(1, `day`);
        }

        return dates;
    }
    catch (err)
    {
        throw err;
    }
}

/**
 * returns the amount of days within a date interval that are outside of the 'notAllowedDays'
 *
 * @param {date} initialDate the starting date
 * @param {date} finalDate the final date
 * @param {array} notAllowedDays (OPTIONAL) a list of week days that should not be counted
 * @returns integer - the amount of days within a date interval that are outside of the 'notAllowedDays'
 */
module.exports.allowedDaysWithin = function (initialDate, finalDate, notAllowedDays) 
{
    try
    {
        let days = 0;
        let endDate = moment(finalDate).set({ hour: 23, minute: 59, second: 59, millisecond: 999 });
        let initDate = moment(initialDate);
        const hasNotAllowedDays = typeUtils.isArrayAndValid(notAllowedDays);

        // finalDate.
        while (initDate < endDate)
        {
            const day = initDate.day();
            if (hasNotAllowedDays)
            {
                if (!notAllowedDays.includes(day))
                    days++;
            }
            else
                days++;

            initDate = initDate.add(1, `day`);
        }

        return days;
    }
    catch (err)
    {
        return 0;
    }
}