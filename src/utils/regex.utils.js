'use strict'
/**
 *mathes a string against a regular expression
 *
 * @param {string} text the text to be matched
 * @param {Regex} regex the evaluation expression
 * @returns an array containing all the matches
 */
module.exports.exec = function (text, regex)
{
    if (!text)
        throw new Error(`RegexUtils exec: you must provide a valid string`);
    if (!regex)
        throw new Error(`RegexUtils exec: you must provide a valid regex`);

    const output = [];
    const matches = text.match(regex) || [];
    for (const match of matches)
        output.push(match);

    return output;
}
/**
 *evaluates a text against a regex
 *
 * @param {string} text the text to be evaluated
 * @param {RegExp} regex the regular expression
 * @returns TRUE only if the WHOLE text matches the given regex
 */
module.exports.apply = function (text, regex)
{
    if (!text)
        throw new Error(`RegexUtils apply: you must provide a valid text, ('${text}' is invalid)`);
    if (!regex)
        throw new Error(`RegexUtils exec: you must provide a valid regex`);

    try
    {
        const matches = text.match(regex) || [];
        for (const match of matches)
            if (match === text)
                return true;

        return false;
    }
    catch (err)    
    {
        return false;
    }
};