'use strict'

const typeUtils = require('./type.utils');
const checkParams = (map, key) =>
{
    if (!map)
        throw new Error(`MapUtils checkParams: invalid 'map' object`);
    if (!(map instanceof Map))
        throw new Error(`MapUtils checkParams: the 'map' MUST BE and instance Map`);
    if (!typeUtils.isValid(key))
        throw new Error(`MapUtils checkParams: invalid 'key' object`);
};
/**
 * gets a value, if the key is defined, from the given map
 *
 * @param {Map} map the map object
 * @param {object} key the key to be evaluated
 * @returns the value stored in the given key
 */
module.exports.get = function (map, key)
{
    checkParams(map, key);

    if (!this.exists(map, key))
        throw new Error(`MapUtils get: the key '${key}' is not defined in the map`);

    const value = map.get(key);
    if (!value)
        value = null;

    return value;
};
/**
 * checks if the given key is stored in the provided map object
 *
 * @param {Map} map the map object
 * @param {object} key the key to be evaluated
 * @returns TRUE if the key is present in the map, false otherwise
 */
module.exports.exists = function (map, key)
{
    checkParams(map, key);
    const keys = Array.from(map.keys());
    return keys.includes(key);
};