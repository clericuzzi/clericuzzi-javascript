const dddByUfList = new Map();
dddByUfList.set("ac", ["68"]);
dddByUfList.set("al", ["81"]);
dddByUfList.set("ap", ["96"]);
dddByUfList.set("am", ["97", "92"]);
dddByUfList.set("ba", ["77", "75", "74", "73", "71"]);
dddByUfList.set("ce", ["88", "85"]);
dddByUfList.set("df", ["61"]);
dddByUfList.set("es", ["28", "27"]);
dddByUfList.set("go", ["62", "64"]);
dddByUfList.set("ma", ["99", "98"]);
dddByUfList.set("mt", ["66", "65"]);
dddByUfList.set("ms", ["67"]);
dddByUfList.set("mg", ["38", "37", "35", "34", "33", "32", "31"]);
dddByUfList.set("pa", ["94", "93", "91"]);
dddByUfList.set("pb", ["83"]);
dddByUfList.set("pr", ["46", "45", "44", "43", "42", "41"]);
dddByUfList.set("pe", ["87", "81"]);
dddByUfList.set("pi", ["89", "86"]);
dddByUfList.set("rj", ["24", "22", "21"]);
dddByUfList.set("rn", ["84"]);
dddByUfList.set("rs", ["55", "54", "53", "51"]);
dddByUfList.set("ro", ["69"]);
dddByUfList.set("rr", ["95"]);
dddByUfList.set("sc", ["49", "48", "47"]);
dddByUfList.set("sp", ["19", "18", "17", "16", "15", "14", "13", "12", "11"]);
dddByUfList.set("se", ["79"]);
dddByUfList.set("to", ["63"]);

const arrayUtils = require('./array.utils');
const stringUtils = require('./string.utils');

const getDdd = uf =>
{
    uf = uf.toLocaleLowerCase();
    if (dddByUfList.has(uf))
    {
        const ufList = dddByUfList.get(uf);

        return arrayUtils.random(ufList);
    }
    else
        throw new Error(`PhoneUtils dddByUf: no such uf as '${uf}'`);
};
const newPhoneNumber = (uf, ddd, mobile) =>
{
    if (uf)
        ddd = getDdd(uf);

    if (mobile)
    {
        const initialDigit = Math.random() > .5 ? 9 : 8;
        return `${ddd}9${initialDigit}${stringUtils.randomDigits(7)}`;
    }
    else
        return `${ddd}3${stringUtils.randomDigits(7)}`;
}

module.exports.dddByUf = uf =>
{
    return getDdd(uf);
};

module.exports.newPhone = ddd =>
{
    return newPhoneNumber(null, ddd, false);;
};
module.exports.newMobile = ddd =>
{
    return newPhoneNumber(null, ddd, true);
};

module.exports.newPhoneByUf = uf =>
{
    return newPhoneNumber(uf, null, false);
};
module.exports.newMobileByUf = uf =>
{
    return newPhoneNumber(uf, null, true);
};