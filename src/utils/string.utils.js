const typeUtils = require('./type.utils');
const dateUtils = require('./date.utils');
const arrayUtils = require('./array.utils');

/**
 * removes any symbols and special characters from the given string and sets it to lowercase
 * @param {string} value the string to have it's symbols and special characters removed
 */
module.exports.skValue = function (value, prefix) 
{
    if (value)
        return `${prefix}|${this.removeSpecialChars(this.removePuctuation(value.toLocaleLowerCase())).replace(' ', '')}`;
    else
        return '';
}
/**
 * removes any special non default characters from the given string
 * @param {string} value the string to have it's non default characters characters removed
 */
module.exports.removeSpecialChars = function (value)
{
    if (value)
    {
        while (value.includes(" "))
            value = value.replace(" ", "");

        value = value
            .replace('à', 'a').replace('â', 'a').replace('ã', 'a').replace('á', 'a').replace('ä', 'a')
            .replace('è', 'e').replace('ê', 'e').replace('é', 'e').replace('ë', 'e')
            .replace('ì', 'i').replace('î', 'i').replace('í', 'i').replace('ï', 'i')
            .replace('ò', 'o').replace('ô', 'o').replace('õ', 'o').replace('ó', 'o').replace('ö', 'o')
            .replace('ù', 'u').replace('û', 'u').replace('ú', 'u').replace('ü', 'u')
            .replace('ç', 'c')
            .replace('ñ', 'n');

        return value;
    }
    else
        return '';
}
/**
 * removes any symbols from the given string
 * @param {string} value the string to have it's symbols characters removed
 */
module.exports.removePuctuation = function (value) 
{
    if (value)
    {
        while (value.includes("  "))
            value = value.replace("  ", " ");

        value = value.replace("'", " ").replace("\"", " ").replace("!", " ").replace("@", " ").replace("#", " ")
            .replace("$", " ").replace("%", " ").replace("¨", " ").replace("&", " ").replace("*", " ")
            .replace("(", " ").replace(")", " ").replace("-", " ").replace("_", " ").replace("+", " ")
            .replace("=", " ").replace("{", " ").replace("}", " ").replace("[", " ").replace("]", " ")
            .replace("`", " ").replace("´", " ").replace("^", " ").replace("~", " ").replace(",", " ")
            .replace("<", " ").replace(".", " ").replace(">", " ").replace(":", " ").replace(";", " ")
            .replace("?", " ").replace("/", " ").replace("°", " ").replace("º", " ").replace("ª", " ")
            .replace("\\", " ").replace("|", " ").replace("¹", " ").replace("²", " ").replace("³", " ")
            .replace("£", " ").replace("¢", " ").replace("¬", " ").replace("§", " ");

        return value;
    }
    else
        return '';
}
/**
 * removes any numbers from the given string
 * @param {string} value the string to have it's numeric characters removed
 */
module.exports.digitsOnly = function (value) 
{
    try
    {
        if (value)
            return value.replace(/(\D|\s)+/g, '');
        else
            return '';
    }
    catch (err)
    {
        console.log(`ERROR`, err);
        return null;
    }
}
/**
 * applies a transformation on the first letter of the given string
 * @param {string} value the string to have it's first letter transformed
 * @returns {string} the given value with the transformation applied
 */
module.exports.firstLetterUppercase = function (value) 
{
    if (typeUtils.isStringAndValid(value))
    {
        if (value.length === 1)
            value = value.charAt(0).toUpperCase();
        else
            value = value.charAt(0).toUpperCase() + value.slice(1);
    }

    return value;
}
/**
 * applies a transformation on the first letter after a underline, modifying it to uper case
 * @param {string} value the string to have it's first letter transformed
 * @param {boolean} fistToUpper if TRUE the first letter will be modified to upper case
 * @returns {string} the given value with the transformation applied
 */
module.exports.underlineToUppercase = function (value, fistToUpper = false) 
{
    const underline = `_`;
    let underlinePosition = value.indexOf(underline);
    while (underlinePosition > 0)
    {
        const nextChar = value.substring(underlinePosition + 1, underlinePosition + 2);
        value = value.substring(0, underlinePosition) + nextChar.toLocaleUpperCase() + value.substring(underlinePosition + 2);

        underlinePosition = value.indexOf(underline);
    }

    if (fistToUpper)
        value = this.firstLetterUppercase(value);

    return value;
}
/**
 * removes all occurrences of double spaces from a given string
 */
module.exports.removeDoubleSpaces = function (value)
{
    if (typeUtils.isStringAndValid(value))
        while (value.indexOf('  ') >= 0)
            value = value.replace('  ', ' ');

    return value;
}

module.exports.formatDate = function (value) 
{
    let formattedValue = '';
    if (value)
    {
        value = this.digitsOnly(value);
        const endsWithZero = value.slice(-1) === `0`;
        const length = value.length;
        if (dateUtils.isValid(value) || endsWithZero)
        {
            if (length < 3)
                formattedValue = value.substring(0, length);
            else if (length < 5)
                formattedValue = `${value.substring(0, 2)}/${value.substring(2, length)}`;
            else if (length < 9)
                formattedValue = `${value.substring(0, 2)}/${value.substring(2, 4)}/${value.substring(4, length)}`;
            else
                return ``;
        }
        else
            formattedValue = this.formatDate(value.substring(0, value.length - 1));
    }

    return formattedValue;
}

module.exports.formatMoney = function (value)
{
    let formattedValue = '';
    if (value)
    {
        value = this.digitsOnly(value);
        const numberValue = Number.parseFloat(value) / 100;
        formattedValue = numberValue.toString();
        if (formattedValue.indexOf(`.`) >= 0)
        {
            const parts = formattedValue.split(`.`);
            while (parts[1].length < 2)
                parts[1] = `${parts[1]}0`;
            formattedValue = parts.join(`.`);
        }
        else
            formattedValue = `${formattedValue}.00`;
    }

    return formattedValue;
};
module.exports.formatNumber = function (value)
{
    let formattedValue = '';
    if (value)
    {
        value = this.digitsOnly(value);
        const numberValue = Number.parseFloat(value).toFixed(0);
        formattedValue = numberValue.toString();
    }

    return formattedValue;
};

module.exports.brCleanPhone = function (value)
{
    if (typeUtils.isStringAndValid(value))
        return this.digitsOnly(value).trim();
    else
        return null;
}
module.exports.brFormatPhone = function (value)
{
    let formattedValue = '';
    if (value)
    {
        value = this.digitsOnly(value);
        const length = value.length;
        switch (length)
        {
            case 1:
                {
                    formattedValue = `${value.substring(0, length)}`;
                    break;
                }
            case 2:
                {
                    formattedValue = `${value.substring(0, length)}`;
                    break;
                }
            case 3:
                {
                    formattedValue = `${value.substring(0, length)}`;
                    break;
                }
            case 4:
                {
                    formattedValue = `${value.substring(0, length)}`;
                    break;
                }
            case 5:
                {
                    formattedValue = `${value.substring(0, 4)}.${value.substring(4, length)}`;
                    break;
                }
            case 6:
                {
                    formattedValue = `${value.substring(0, 4)}.${value.substring(4, length)}`;
                    break;
                }
            case 7:
                {
                    formattedValue = `${value.substring(0, 4)}.${value.substring(4, length)}`;
                    break;
                }
            case 8:
                {
                    formattedValue = `${value.substring(0, 4)}.${value.substring(4, length)}`;
                    break;
                }
            case 9:
                {
                    formattedValue = `${value.substring(0, 5)}.${value.substring(5, length)}`;
                    break;
                }

            default:
                {
                    formattedValue = ``;
                    break;
                }
        }
    }

    return formattedValue;
}
module.exports.brFormatPhoneDdd = function (value)
{
    let formattedValue = '';
    if (value)
    {
        value = this.digitsOnly(value);
        const length = value.length;
        switch (length)
        {
            case 1:
                {
                    formattedValue = `(${value.substring(0, length)}`;
                    break;
                }
            case 2:
                {
                    formattedValue = `(${value.substring(0, length)}`;
                    break;
                }
            case 3:
                {
                    formattedValue = `(${value.substring(0, 2)}) ${value.substring(2, length)}`;
                    break;
                }
            case 4:
                {
                    formattedValue = `(${value.substring(0, 2)}) ${value.substring(2, length)}`;
                    break;
                }
            case 5:
                {
                    formattedValue = `(${value.substring(0, 2)}) ${value.substring(2, length)}`;
                    break;
                }
            case 6:
                {
                    formattedValue = `(${value.substring(0, 2)}) ${value.substring(2, length)}`;
                    break;
                }
            case 7:
                {
                    formattedValue = `(${value.substring(0, 2)}) ${value.substring(2, 6)}.${value.substring(6, length)}`;
                    break;
                }
            case 8:
                {
                    formattedValue = `(${value.substring(0, 2)}) ${value.substring(2, 6)}.${value.substring(6, length)}`;
                    break;
                }
            case 9:
                {
                    formattedValue = `(${value.substring(0, 2)}) ${value.substring(2, 6)}.${value.substring(6, length)}`;
                    break;
                }
            case 10:
                {
                    formattedValue = `(${value.substring(0, 2)}) ${value.substring(2, 6)}.${value.substring(6, length)}`;
                    break;
                }
            case 11:
                {
                    formattedValue = `(${value.substring(0, 2)}) ${value.substring(2, 7)}.${value.substring(7, length)}`;
                    break;
                }

            default:
                {
                    formattedValue = ``;
                    break;
                }
        }
    }

    return formattedValue;
}
module.exports.brFormatPhoneFull = function (value)
{
    let formattedValue = '';
    if (value)
    {
        value = this.digitsOnly(value);
        const length = value.length;
        switch (length)
        {
            case 1:
                {
                    formattedValue = `+${value.substring(0, length)}`;
                    break;
                }
            case 2:
                {
                    formattedValue = `+${value.substring(0, length)}`;
                    break;
                }
            case 3:
                {
                    formattedValue = `+${value.substring(0, 2)} (${value.substring(2, length)}`;
                    break;
                }
            case 4:
                {
                    formattedValue = `+${value.substring(0, 2)} (${value.substring(2, length)})`;
                    break;
                }
            case 5:
                {
                    formattedValue = `+${value.substring(0, 2)} (${value.substring(2, 4)}) ${value.substring(4, length)}`;
                    break;
                }
            case 6:
                {
                    formattedValue = `+${value.substring(0, 2)} (${value.substring(2, 4)}) ${value.substring(4, length)}`;
                    break;
                }
            case 7:
                {
                    formattedValue = `+${value.substring(0, 2)} (${value.substring(2, 4)}) ${value.substring(4, length)}`;
                    break;
                }
            case 8:
                {
                    formattedValue = `+${value.substring(0, 2)} (${value.substring(2, 4)}) ${value.substring(4, length)}`;
                    break;
                }
            case 9:
                {
                    formattedValue = `+${value.substring(0, 2)} (${value.substring(2, 4)}) ${value.substring(4, 8)}.${value.substring(8, length)}`;
                    break;
                }
            case 10:
                {
                    formattedValue = `+${value.substring(0, 2)} (${value.substring(2, 4)}) ${value.substring(4, 8)}.${value.substring(8, length)}`;
                    break;
                }
            case 11:
                {
                    formattedValue = `+${value.substring(0, 2)} (${value.substring(2, 4)}) ${value.substring(4, 8)}.${value.substring(8, length)}`;
                    break;
                }
            case 12:
                {
                    formattedValue = `+${value.substring(0, 2)} (${value.substring(2, 4)}) ${value.substring(4, 8)}.${value.substring(8, length)}`;
                    break;
                }
            case 13:
                {
                    formattedValue = `+${value.substring(0, 2)} (${value.substring(2, 4)}) ${value.substring(4, 9)}.${value.substring(9, length)}`;
                    break;
                }

            default:
                {
                    formattedValue = ``;
                    break;
                }
        }
    }

    return formattedValue;
}
module.exports.brFormatCpf = function (value)
{
    let formattedValue = '';
    if (value)
    {
        value = this.digitsOnly(value);
        const length = value.length;
        switch (length)
        {
            case 11:
                {
                    formattedValue = `${value.substring(0, 3)}.${value.substring(3, 6)}.${value.substring(6, 9)}-${value.substring(9, 11)}`;
                    break;
                }
            case 10:
                {
                    formattedValue = `${value.substring(0, 3)}.${value.substring(3, 6)}.${value.substring(6, 9)}-${value.substring(9, 10)}`;
                    break;
                }
            case 9:
                {
                    formattedValue = `${value.substring(0, 3)}.${value.substring(3, 6)}.${value.substring(6, 9)}`;
                    break;
                }
            case 8:
                {
                    formattedValue = `${value.substring(0, 3)}.${value.substring(3, 6)}.${value.substring(6, 8)}`;
                    break;
                }
            case 7:
                {
                    formattedValue = `${value.substring(0, 3)}.${value.substring(3, 6)}.${value.substring(6, 7)}`;
                    break;
                }
            case 6:
                {
                    formattedValue = `${value.substring(0, 3)}.${value.substring(3, 6)}`;
                    break;
                }
            case 5:
                {
                    formattedValue = `${value.substring(0, 3)}.${value.substring(3, 5)}`;
                    break;
                }
            case 4:
                {
                    formattedValue = `${value.substring(0, 3)}.${value.substring(3, 4)}`;
                    break;
                }
            case 3:
                {
                    formattedValue = `${value.substring(0, 3)}`;
                    break;
                }
            case 2:
                {
                    formattedValue = `${value.substring(0, 2)}`;
                    break;
                }
            case 1:
                {
                    formattedValue = `${value.substring(0, 1)}`;
                    break;
                }
            default:
                {
                    formattedValue = value;
                    break;
                }
        }
    }

    return formattedValue;
}
module.exports.brFormatZip = function (value)
{
    let formattedValue = '';
    if (value)
    {
        value = this.digitsOnly(value);
        const length = value.length;
        switch (length)
        {
            case 8:
                {
                    formattedValue = `${value.substring(0, 5)}-${value.substring(5, 8)}`;
                    break;
                }
            case 7:
                {
                    formattedValue = `${value.substring(0, 5)}-${value.substring(5, 7)}`;
                    break;
                }
            case 6:
                {
                    formattedValue = `${value.substring(0, 5)}-${value.substring(5, 6)}`;
                    break;
                }
            case 5:
                {
                    formattedValue = `${value.substring(0, 5)}`;
                    break;
                }
            case 4:
                {
                    formattedValue = `${value.substring(0, 4)}`;
                    break;
                }
            case 3:
                {
                    formattedValue = `${value.substring(0, 3)}`;
                    break;
                }
            case 2:
                {
                    formattedValue = `${value.substring(0, 2)}`;
                    break;
                }
            case 1:
                {
                    formattedValue = `${value.substring(0, 1)}`;
                    break;
                }
            default:
                {
                    formattedValue = '';
                    break;
                }
        }
    }

    return formattedValue;
}
module.exports.brFormatCnpj = function (value)
{
    let formattedValue = '';
    if (value)
    {
        value = this.digitsOnly(value);
        const length = value.length;
        switch (length)
        {
            case 14:
                {
                    formattedValue = `${value.substring(0, 2)}.${value.substring(2, 5)}.${value.substring(5, 8)}/${value.substring(8, 12)}-${value.substring(12, 14)}`;
                    break;
                }
            case 13:
                {
                    formattedValue = `${value.substring(0, 2)}.${value.substring(2, 5)}.${value.substring(5, 8)}/${value.substring(8, 12)}-${value.substring(12, 13)}`;
                    break;
                }
            case 12:
                {
                    formattedValue = `${value.substring(0, 2)}.${value.substring(2, 5)}.${value.substring(5, 8)}/${value.substring(8, 12)}`;
                    break;
                }
            case 11:
                {
                    formattedValue = `${value.substring(0, 2)}.${value.substring(2, 5)}.${value.substring(5, 8)}/${value.substring(8, 11)}`;
                    break;
                }
            case 10:
                {
                    formattedValue = `${value.substring(0, 2)}.${value.substring(2, 5)}.${value.substring(5, 8)}/${value.substring(8, 10)}`;
                    break;
                }
            case 9:
                {
                    formattedValue = `${value.substring(0, 2)}.${value.substring(2, 5)}.${value.substring(5, 8)}/${value.substring(8, 9)}`;
                    break;
                }
            case 8:
                {
                    formattedValue = `${value.substring(0, 2)}.${value.substring(2, 5)}.${value.substring(5, 8)}`;
                    break;
                }
            case 7:
                {
                    formattedValue = `${value.substring(0, 2)}.${value.substring(2, 5)}.${value.substring(5, 7)}`;
                    break;
                }
            case 6:
                {
                    formattedValue = `${value.substring(0, 2)}.${value.substring(2, 5)}.${value.substring(5, 6)}`;
                    break;
                }
            case 5:
                {
                    formattedValue = `${value.substring(0, 2)}.${value.substring(2, 5)}`;
                    break;
                }
            case 4:
                {
                    formattedValue = `${value.substring(0, 2)}.${value.substring(2, 4)}`;
                    break;
                }
            case 3:
                {
                    formattedValue = `${value.substring(0, 2)}.${value.substring(2, 3)}`;
                    break;
                }
            case 2:
                {
                    formattedValue = `${value.substring(0, 2)}`;
                    break;
                }
            case 1:
                {
                    formattedValue = `${value.substring(0, 1)}`;
                    break;
                }
            default:
                {
                    formattedValue = value;
                    break;
                }
        }
    }

    return formattedValue;
}

module.exports.replace = function (value, ...parameters)
{
    if (Array.isArray(parameters))
    {
        const wildCard = _stringTokens.Wildcard;
        const wildCardLength = wildCard.length;
        for (const param of parameters)
        {
            const wildCardIndex = value.indexOf(_stringTokens.Wildcard);
            if (wildCardIndex >= 0)
                value = value.substring(0, wildCardIndex) + param + value.substring(wildCardIndex + wildCardLength);
        }
    }

    return value;
}

module.exports.validPhone = function (phone, countryCode)
{
    if (!phone)
        return null;

    phone = this.digitsOnly(phone);
    if (countryCode)
        phone = countryCode + phone;

    phone = `+${phone}`;

    return phone;
}

module.exports.Tokens = function () { return _stringTokens; }


const all = `ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789`;
const chars = `ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz`;
const digits = `0123456789`;
const charsLower = `abcdefghijklmnopqrstuvwxyz`;
const charsUpper = `ABCDEFGHIJKLMNOPQRSTUVWXYZ`;
const random = function (source, length, canRepeat)
{
    const array = source.split(``);
    const values = [];
    while (values.length < length)
    {
        const value = arrayUtils.random(array);
        values.push(value);
        if (!canRepeat)
        {
            arrayUtils.remove(array, value);
            if (array.length === 0)
                break;
        }
    }

    return values.join(``);
}
module.exports.random = function (length, canRepeat = true)
{
    return random(all, length, canRepeat);
};
module.exports.randomDigits = function (length, canRepeat = true)
{
    return random(digits, length, canRepeat);
};
module.exports.randomChars = function (length, canRepeat = true)
{
    return random(chars, length, canRepeat);
};
module.exports.randomCharsLower = function (length, canRepeat = true)
{
    return random(charsLower, length, canRepeat);
};
module.exports.randomCharsUpper = function (length, canRepeat = true)
{
    return random(charsUpper, length, canRepeat);
};

const _stringTokens =
{
    Wildcard: '|_@_|',
}
Object.freeze(_stringTokens);