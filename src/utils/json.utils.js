const typeUtils = require('./type.utils');

const testIfString = function (json)
{
    const objectType = typeof json;
    if (objectType === `string`)
    {
        try
        {
            let parsedObject = JSON.parse(json);
            if (typeUtils.isObject(parsedObject))
                return parsedObject;

            return json;
        }
        catch (err)
        {
            return json;
        }
    }
    else
        return json;
}

module.exports.parse = function (json, baseObject = {})
{
    if (!typeUtils.isValid(json))
        return null;

    json = testIfString(json);
    if (typeUtils.isArray(json))
    {
        const newArray = [];
        const items = json;
        for (const item of items)
            newArray.push(this.parse(item));

        return newArray;
    }
    else if (typeUtils.isObject(json))
    {
        const newObject = {};
        const keys = Object.keys(json);
        for (const key of keys)
        {
            if (typeUtils.isArray(json[key]))
            {
                newObject[key] = [];
                const items = json[key];
                for (const item of items)
                    newObject[key].push(this.parse(item));
            }
            else
            {
                try
                {
                    newObject[key] = JSON.parse(json[key]);
                    if (typeUtils.isArray(newObject[key]) || typeUtils.isObject(newObject[key]))
                        newObject[key] = this.parse(newObject[key]);
                }
                catch (err)
                {
                    newObject[key] = json[key];
                }
            }
        }

        return Object.assign(baseObject, newObject);
    }
    else
    {
        try
        {
            return this.parse(JSON.parse(json));
        }
        catch (error)
        {
            return json;
        }
    }
}
module.exports.stringify = function (obj)
{
    if (!typeUtils.isValid(obj))
        return null;

    if (typeUtils.isObject(obj))
    {
        const jsonObj = {};
        const keys = Object.keys(obj);
        for (const key of keys)
        {
            const currentValue = obj[key];
            if (!typeUtils.isFunction(currentValue) && typeUtils.isJsonValid(currentValue))
            {
                if (typeUtils.isArray(currentValue))
                    jsonObj[key] = currentValue.map(i => this.stringify(i));
                else if (typeUtils.isObject(currentValue))
                    jsonObj[key] = this.stringify(currentValue);
                else
                    jsonObj[key] = JSON.stringify(currentValue);
            }
        }
        return JSON.stringify(jsonObj);
    }
    else
    {
        try
        {
            return JSON.stringify(obj);
        }
        catch (err)
        {
            return obj;
        }
    }
}