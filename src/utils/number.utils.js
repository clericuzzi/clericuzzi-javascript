'use strict'

let defaultDecimalCases = 2;
let defaultCurrencySymbol = `R$`;
let defaultGroupSeparator = `.`;
let defaultDecimalSeparator = `,`;
const typeUtils = require('./type.utils');
module.exports = class NumberUtils
{
    /**
     * formats a given value a whole number
     * @param {number} value the number to be formatted
     * @param {string} groupSeparator (OPTIONAL) the character that will be used as a group separator, default is '.'
     * @returns the provided value as a whole number string 
     */
    static toAmount(value, groupSeparator = null)
    {
        if (!typeUtils.isValid(value))
            throw new Error(`NumberUtils toAmount: invalid argument '${value}'`);

        return `${value.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, `$1${groupSeparator || defaultGroupSeparator}`)}`;
    }
    /**
     * formats a given value as currency
     * @param {number} value the number to be formatted
     * @param {number} decimalCases (OPTIONAL) the amount of decimal cases, default is '2'
     * @param {string} currencySymbol (OPTIONAL) the currency symbol that will be shown in the returned string, default is 'R$'
     * @param {string} groupSeparator (OPTIONAL) the character that will be used as a group separator, default is '.'
     * @param {string} decimalSeparator (OPTIONAL) the character that will be used as a decimal separator, default is ','
     * @returns the provided value as a currency string 
     */
    static toCurrency(value, decimalCases = null, currencySymbol = null, groupSeparator = null, decimalSeparator = null)
    {
        if (!typeUtils.isValid(value))
            throw new Error(`NumberUtils toCurrency: invalid argument '${value}'`);

        return `${currencySymbol || defaultCurrencySymbol} ${value.toFixed(decimalCases || defaultDecimalCases).replace('.', decimalSeparator || defaultDecimalSeparator).replace(/(\d)(?=(\d{3})+(?!\d))/g, `$1${groupSeparator || defaultGroupSeparator}`)}`;
    }
    /**
     * formats a given value as a percentage
     * @param {number} value the number to be formatted
     * @param {bool} multiply (OPTIONAL) if TRUE the given value will be multiplied by 100, default is true
     * @param {number} decimalCases (OPTIONAL) the amount of decimal cases, default is '2'
     * @param {string} groupSeparator (OPTIONAL) the character that will be used as a group separator, default is '.'
     * @param {string} decimalSeparator (OPTIONAL) the character that will be used as a decimal separator, default is ','
     * @returns the provided value as a percentage string 
     */
    static toPercentage(value, multiply = true, decimalCases = null, groupSeparator = null, decimalSeparator = null)
    {
        if (!typeUtils.isValid(value))
            throw new Error(`NumberUtils toPercentage: invalid argument '${value}'`);

        if (multiply)
            value *= 100;
        return `${value.toFixed(decimalCases || defaultDecimalCases).replace('.', decimalSeparator || defaultDecimalSeparator).replace(/(\d)(?=(\d{3})+(?!\d))/g, `$1${groupSeparator || defaultGroupSeparator}`)}%`;
    }
}