let _time = null;

module.exports.setTimer = () =>
{
    _time = new Date().getTime();
};
module.exports.logTimer = (label, ...data) =>
{
    if (!_time)
        console.log(`ProcessUtils logTime: Timer has not been initialized, please call 'setTimer' before trying to log time`);
    else
        console.log(`${label}: ${(new Date().getTime() - _time)}ms\t`, ...data);
};
module.exports.sleep = async (duration) =>
{
    await new Promise(r => setTimeout(r, duration));
};
module.exports.race = async (timeout, raceName, ...promises) => 
{
    promises.push(raceTimer(raceName, timeout));
    const value = await Promise.race(promises).then(i => i).catch(e => e);

    if (value instanceof Error)
        throw value;
    else
        return value;
};
const raceTimer = (raceName, timeout = 10000) => new Promise((_, reject) => setTimeout(reject, timeout, new Error(`${raceName} timed out!`)));
