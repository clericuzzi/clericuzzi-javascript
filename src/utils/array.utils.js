const typeUtils = require('./type.utils');
const distinctFilter = function (value, index, self) 
{
    return self.indexOf(value) === index;
}

module.exports.sumProduct = function (array1, array2) 
{
    let result = 0;
    for (let i = 0; i < array1.length; i++)
        result += array1[i] * array2[i];

    return result;
};

module.exports.mapProperty = function (array, property) { return array.map(i => i[property]); }

module.exports.max = function (array) { return Math.max(...array); }
module.exports.min = function (array) { return Math.min(...array); }
module.exports.sum = function (array) { return array.reduce((x, y) => x + y, 0); }
module.exports.avg = function (array) { return this.sum(array) / array.length; }
module.exports.maxByProperty = function (array, property) { return Math.max(array.map(i => i[property])); }
module.exports.minByProperty = function (array, property) { return Math.min(array.map(i => i[property])); }
module.exports.sumByProperty = function (array, property) { return this.sum(array.map(i => i[property])); }
module.exports.avgByProperty = function (array, property) { return this.sumByProperty(array, property) / array.length; }

module.exports.sortAsc = function (array) { return array.sort((x, y) => x > y ? 1 : -1); }
module.exports.sortDesc = function (array) { return array.sort((x, y) => x < y ? 1 : -1); }
module.exports.sortAscByProperty = function (array, property) { return array.sort((x, y) => x[property] > y[property] ? 1 : -1); }
module.exports.sortDescByProperty = function (array, property) { return array.sort((x, y) => x[property] < y[property] ? 1 : -1); }

module.exports.random = function (array) { return array[Math.floor(Math.random() * array.length)]; }

module.exports.distinct = function (array) { return array.filter(distinctFilter); }
module.exports.distinctSortAsc = function (array) { return this.distinct(array.sort((x, y) => x > y ? 1 : -1)); }
module.exports.distinctSortDesc = function (array) { return this.distinct(array.sort((x, y) => x < y ? 1 : -1)); }
module.exports.distinctSortAscByProperty = function (array, property) { return this.distinct(array.sort((x, y) => x[property] > y[property] ? 1 : -1)); }
module.exports.distinctSortDescByProperty = function (array, property) { return this.distinct(array.sort((x, y) => x[property] < y[property] ? 1 : -1)); }

/**
 * removes the given item from the array
 *
 * @param {array} array the array to be worked on
 * @param {object} item the item to be removed, if found
 */
module.exports.remove = function (array, item) 
{
    const indexOf = array.indexOf(item);
    if (indexOf >= 0)
        array.splice(indexOf, 1);
    else
        throw new Error(`no such item in the array`);
}
/**
 * removes an item from the given array that matches the search criteria
 *
 * @param {array} array the array to be worked on
 * @param {string} property the field that will be compared in search of the given value
 * @param {object} value the value to be looked for
 */
module.exports.removeByProperty = function (array, property, value) 
{
    const item = array.find(i => i[property] === value);
    if (!item)
        throw new Error(`no item with '${property}' equals to '${value} found in the given array'`);

    this.remove(array, item);
}

module.exports.setRank = function (items, itemProperty, rankingProperty) 
{
    items = this.sortDescByProperty(items, itemProperty);

    let index = 1;
    let currentIndex = 0;
    let currentValue = null;
    for (let item of items)
    {
        const actualValue = item[itemProperty];
        if (actualValue !== currentValue)
        {
            currentIndex = index;
            item[rankingProperty] = index;
            currentValue = actualValue;
        }
        else
            item[rankingProperty] = currentIndex;

        index++;
    }

    return items;
}

module.exports.asyncFilter = async function (array, filterFunction) 
{
    const newArray = [];
    if (Array.isArray(array) && array.length > 0 && typeof filterFunction === 'function')
    {
        for (const item of array)
        {
            const result = await filterFunction(item);
            if (result === true)
                newArray.push(item);
        }
    }

    return newArray;
}
/**
 * shifts the giten item to the left, if possible
 *
 * @param {array} array the array to be manipulated
 * @param {object} item the item to have it's position left sifted
 */
module.exports.shiftLeft = function (array, item)
{
    if (!typeUtils.isArrayAndValid(array))
        throw new Error(`the is given is invalid for a 'shiftLeft' operation'`);

    if (!typeUtils.isValid(item))
        throw new Error(`'shiftLeft' invalid item given`);

    const itemIndex = array.indexOf(item);
    if (itemIndex < 0)
        throw new Error(`'shiftLeft' the item is not present in the given array`);
    else if (itemIndex == 0)
        throw new Error(`'shiftLeft' cannot be performed in the first item of the array`);
    else
    {
        const temp = array[itemIndex];
        array[itemIndex] = array[itemIndex - 1];
        array[itemIndex - 1] = temp;

        return array;
    }
}
/**
 * shifts the giten item to the right, if possible
 *
 * @param {array} array the array to be manipulated
 * @param {object} item the item to have it's position right sifted
 */
module.exports.shiftRight = function (array, item)
{
    if (!typeUtils.isArrayAndValid(array))
        throw new Error(`the is given is invalid for a 'shiftRight' operation'`);

    if (!typeUtils.isValid(item))
        throw new Error(`'shiftRight' invalid item given`);

    const itemIndex = array.indexOf(item);
    if (itemIndex < 0)
        throw new Error(`'shiftRight' the item is not present in the given array`);
    else if (itemIndex >= array.length)
        throw new Error(`'shiftRight' cannot be performed in the last item of the array`);
    else
    {
        const temp = array[itemIndex];
        array[itemIndex] = array[itemIndex + 1];
        array[itemIndex + 1] = temp;

        return array;
    }
}
/**
 * loops throught all items in the array looking for a match with the newItem by the given property
 * the first match will be replace by the new item
 * if no item is foud an error will be thrown
 *
 * @param {Array} array the array to be manipulated
 * @param {object} newItem the item to be added IF we find a match by the given property
 * @param {string} property the proverty to be evaluated in each item of the array, the first match will be replaced by the newItem
 */
module.exports.replaceByProperty = function (array, newItem, property)
{
    if (!typeUtils.isArrayAndValid(array))
        throw new Error(`ArrayUtils replaceByProperty:the is given is invalid for a 'shiftRight' operation'`);

    if (!typeUtils.isValid(newItem))
        throw new Error(`ArrayUtils replaceByProperty: invalid item given`);

    if (!typeUtils.isStringAndValid(property))
        throw new Error(`ArrayUtils replaceByProperty: invalid property given`);

    const value = newItem[property];
    const existingItem = array.find(i => i[property] == value);
    if (!existingItem)
        throw new Error(`ArrayUtils replaceByProperty: there is no item where '${property}' is equals to '${value}'`);

    const index = array.indexOf(existingItem);
    this.remove(array, existingItem);
    array.splice(index, 0, newItem);
}
/**
 * loops throught all items in the array looking for a match with the newItem by the given property
 * the first match will be replaced by the new item
 * if no item is found, the new item will be appended to the array
 *
 * @param {Array} array the array to be manipulated
 * @param {object} newItem the item to be added IF we find a match by the given property
 * @param {string} property the proverty to be evaluated in each item of the array, the first match will be replaced by the newItem
 */
module.exports.replaceByPropertyOrAppend = function (array, newItem, property)
{
    if (!typeUtils.isArray(array))
        throw new Error(`ArrayUtils replaceByProperty:the is given is invalid for a 'shiftRight' operation'`);

    if (!typeUtils.isValid(newItem))
        throw new Error(`ArrayUtils replaceByProperty: invalid item given`);

    if (!typeUtils.isStringAndValid(property))
        throw new Error(`ArrayUtils replaceByProperty: invalid property given`);

    const value = newItem[property];
    const existingItem = array.find(i => i[property] == value);
    if (!existingItem)
        array.push(newItem);
    else
    {
        const index = array.indexOf(existingItem);
        this.remove(array, existingItem);
        array.splice(index, 0, newItem);
    }
}

/**
 * brings the given 'item' to the left of the 'reference' one
 *
 * @param {Array} array the array to be manipulated
 * @param {object} reference the item to serve as the index reference
 * @param {object} item the item that will be moved
 */
module.exports.bringLeft = function (array, reference, item)
{
    if (!typeUtils.isArrayAndValid(array))
        throw new Error(`ArrayUtils bringLeft: invalid array given`);

    if (!typeUtils.isValid(reference))
        throw new Error(`ArrayUtils bringLeft: invalid reference given`);

    if (!typeUtils.isValid(item))
        throw new Error(`ArrayUtils bringLeft: invalid item given`);

    const index = array.indexOf(reference);

    this.remove(array, item);
    array.splice(index, 0, item);
}
/**
 * brings the given 'item' to the right of the 'reference' one
 *
 * @param {Array} array the array to be manipulated
 * @param {object} reference the item to serve as the index reference
 * @param {object} item the item that will be moved
 */
module.exports.bringRight = function (array, reference, item)
{
    if (!typeUtils.isArrayAndValid(array))
        throw new Error(`ArrayUtils bringRight: invalid array given`);

    if (!typeUtils.isValid(reference))
        throw new Error(`ArrayUtils bringRight: invalid reference given`);

    if (!typeUtils.isValid(item))
        throw new Error(`ArrayUtils bringRight: invalid item given`);

    const index = array.indexOf(reference);

    this.remove(array, item);
    array.splice(index + 1, 0, item);
}
/**
 *gets the nth item in the given array
 *
 * @param {Array} array the array to be manipulated
 * @param {number} index the position to get the item from
 * @returns the item stored in the given position in the array
 */
module.exports.getByIndex = function (array, index)
{
    if (!typeUtils.isArrayAndValid(array))
        throw new Error(`ArrayUtils getByIndex: invalid array given`);

    if (index >= array.length)
        throw new Error(`ArrayUtils getByIndex: the position given is greater than the given array's`);

    return array.slice(index, index + 1)[0];
}

/**
 * extracts a set of properties from the array
 *
 * @param {any} array the array to be manipulated
 * @param {string[]} properyList a list of properties that are going to be present in the output array
 * @returns a new array with the same objects, but composed only the properties given in the list
 */
module.exports.getPropertySet = function (array, properyList)
{
    if (!typeUtils.isArrayAndValid(array))
        throw new Error(`ArrayUtils getPropertySet: invalid array given`);

    if (!typeUtils.isArrayAndValid(properyList))
        throw new Error(`ArrayUtils getPropertySet: no properties given`);

    const newData = [];
    for (let item of array)
    {
        const newItem = {};
        for (let property of properyList)
            newItem[property] = item[property] || null;

        newData.push(newItem);
    }

    return newData;
};

/**
 * takes an object and returs an array containing all of it's property values
 *
 * @param {any} obj the object to be handled
 * @returns {any[]} an array containing all values stored in the given object
 */
module.exports.objectToPropertyList = function (obj)
{
    const values = [];
    const keys = Object.keys(bill);
    for (let key of keys)
        values.push(bill[key]);

    return values;
}

/**
 * returns all distict values for a given property
 *
 * @param {[*]} array an array of items
 * @param {string} property the property that will be read in each item
 * @returns {[*]} an array containing all possible values for the given property
 */
module.exports.distinctPropertyValues = function (array, property)
{
    if (!Array.isArray(array))
        throw new Error(`ArrayUtils distinctPropertyValues: invalid array provided (${array})`);

    if (!property)
        throw new Error(`ArrayUtils distinctPropertyValues: invalid property provided ('${property}')`);

    array = this.flatten(array);
    const returnValues = array.map(i => i[property] ? i[property] : Infinity);

    return returnValues.filter(distinctFilter).filter(i => i !== Infinity);
};

/**
 * flattens out the given array
 *
 * @param {[*]} input the array to be flattened
 * @returns {[*]} the flattened result of the input array
 */
module.exports.flatten = input => flattenAction(input);
/**
 * actually flattens out the input array
 *
 * @param {*} input the array to be flattened
 * @returns {[*]} a flat version of the input array
 */
const flattenAction = input =>
{
    return input.reduce(function (flat, toFlatten)
    {
        return flat.concat(Array.isArray(toFlatten) ? flattenAction(toFlatten) : toFlatten);
    }, []);
}