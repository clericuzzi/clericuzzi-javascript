const typeUtils = require('../utils/type.utils');
const stringFormatter = require('../utils/string.utils');

// enums
const PhoneType = require('../business/enums/phoneType.enum');

const _EscapeCharacterFinder = /(\[.\])+/;

const _brRegexes = {
    Cpf: /\d{11}/g,
    Zip: /\d{8}/g,
    Cnpj: /\d{14}/g,
    Money: /(\d{1,3})([.](\d{1,3}))*[,]\d{2}/,

    Date: /(\d{2}\/\d{2})\/(\d{4})/,
    Time: /(\d{5}|\d{4})\.(\d{4})/,
    DateTime: /(\d{5}|\d{4})\.(\d{4})/,

    Phone: /\d{9}|\d{8}/g,
    PhoneDdd: /\d{11}|\d{10}/g,
    PhoneFull: /\d{13}|\d{12}/g,
};
Object.freeze(_brRegexes);
module.exports.BrRegexes = _brRegexes;

const _GeneralExpressions = {
    Email: /[a-z0-9\._%+!$&*=^|~#%'`?{}/\-]+@([a-z0-9\-]+\.){1,}([a-z]{2,16})/,
    PasswordLight: /.{4,16}/,
    PasswordMedium: /(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[a-zA-Z0-9\D]{8,16}$/,
    PasswordSecure: /(?=.*[0-9])(?=.*['"!@#$%¨&*()_\-+=§¹²³£¢¬/?°|\\<,>.:;])(?=.*[a-z])(?=.*[A-Z])[a-zA-Z0-9'"!@#$%¨&*()_\-+=§¹²³£¢¬/?°|\\<,>.:;]{8,32}$/,
};
Object.freeze(_GeneralExpressions);

module.exports.nullOrEmpty = function (value) { return value === null || value === undefined || value === ''; }
module.exports.EscapeFinder = function (char) { return _EscapeCharacterFinder.replace(`@`, char); }

module.exports.GeneralExpressions = function () { return _GeneralExpressions; }

module.exports.brValidateCpf = function (value) { return _brRegexes.Cpf.test(value); }
module.exports.brValidateZip = function (value) { return _brRegexes.Zip.test(value); }
module.exports.brValidateCnpj = function (value) { return _brRegexes.Cnpj.test(value); }
module.exports.brValidateMoney = function (value) { return _brRegexes.Money.test(value); }

module.exports.brValidatePhone = function (value) 
{
    if (typeUtils.isStringAndValid(value))
    {
        value = stringFormatter.digitsOnly(value);

        return typeUtils.isStringAndValid(value) && (value.length >= 8 && value.length <= 13);
    }
    else
        return false;
}
module.exports.brValidatePhoneType = function (value) 
{
    if (typeUtils.isStringAndValid(value) && (value.length >= 8 && value.length <= 13))
    {
        let initialCharacter = null;
        if (value.length < 10)
            initialCharacter = value.substring(0, 1);
        else if (value.length < 12)
            initialCharacter = value.substring(2, 1);
        else
            initialCharacter = value.substring(4, 1);

        if (initialCharacter === `9` || initialCharacter === `8` || initialCharacter === `7`)
            return PhoneType.Mobile;
        else
            return PhoneType.Land;
    }
    else
        return null;
}

module.exports.brEnforceCpf = function (value) { return _brRegexes.Cpf.test(value); }

module.exports.charsOnly = function (value) 
{
    const regex = /\d+/;
    let match = regex.exec(value);
    while ((match = regex.exec(value)) !== null)
        value = value.replace(match[0], '');

    return value;
}
module.exports.digitsOnly = function (value) 
{
    const regex = /\D+/;
    let match = regex.exec(value);
    while ((match = regex.exec(value)) !== null)
        value = value.replace(match[0], '');

    return value;
}

module.exports.validateEmail = function (email) 
{
    if (!email)
        throw new Error(`StringsValidator validateEmail: no data provided`);

    return _GeneralExpressions.Email.test(email);
}
module.exports.validatePasswordLight = function (password) 
{
    if (!password)
        throw new Error(`StringsValidator validatePasswordLight: no data provided`);

    return _GeneralExpressions.PasswordLight.test(password);
}
module.exports.validatePasswordMedium = function (password) 
{
    if (!password)
        throw new Error(`StringsValidator validatePasswordMedium: no data provided`);

    return _GeneralExpressions.PasswordMedium.test(password);
}
module.exports.validatePasswordSecure = function (password) 
{
    if (!password)
        throw new Error(`StringsValidator validatePasswordSecure: no data provided`);

    return _GeneralExpressions.PasswordSecure.test(password);
}
module.exports.matchingPasswords = function (model, passwordProperty, confirmationProperty) 
{
    if (model && passwordProperty && confirmationProperty)
        return model[passwordProperty] === model[confirmationProperty];
    else
        return null;
}

/**
 * validates a given cpf
 *
 * @param {*} cpf the cpf to be validated
 * @returns TRUE if the given cpf is valid, false otherwise
 */
module.exports.checkCpf = function (cpf)
{
    let sum = 0;
    let rest = 0;
    if (cnpj === "00000000000" ||
        cnpj === "11111111111" ||
        cnpj === "22222222222" ||
        cnpj === "33333333333" ||
        cnpj === "44444444444" ||
        cnpj === "55555555555" ||
        cnpj === "66666666666" ||
        cnpj === "77777777777" ||
        cnpj === "88888888888" ||
        cnpj === "99999999999")
        return false;

    for (let i = 1; i <= 9; i++)
        sum = sum + parseInt(cpf.substring(i - 1, i)) * (11 - i);
    rest = (sum * 10) % 11;

    if ((rest === 10) || (rest === 11))
        rest = 0;
    if (rest != parseInt(cpf.substring(9, 10)))
        return false;

    sum = 0;
    for (let i = 1; i <= 10; i++)
        sum = sum + parseInt(cpf.substring(i - 1, i)) * (12 - i);
    rest = (sum * 10) % 11;

    if ((rest === 10) || (rest === 11))
        rest = 0;
    if (rest != parseInt(cpf.substring(10, 11)))
        return false;

    return true;
}

/**
 * validates a given cnpj
 *
 * @param {*} cnpj the cnpj to be validated
 * @returns TRUE if the given cnpj is valid, false otherwise
 */
module.exports.checkCnpj = function (cnpj)
{
    cnpj = cnpj.replace(/[^\d]+/g, '');

    if (cnpj === '')
        return false;

    if (cnpj.length != 14)
        return false;

    if (cnpj === "00000000000000" ||
        cnpj === "11111111111111" ||
        cnpj === "22222222222222" ||
        cnpj === "33333333333333" ||
        cnpj === "44444444444444" ||
        cnpj === "55555555555555" ||
        cnpj === "66666666666666" ||
        cnpj === "77777777777777" ||
        cnpj === "88888888888888" ||
        cnpj === "99999999999999")
        return false;

    let size = cnpj.length - 2
    let numbers = cnpj.substring(0, size);
    let digits = cnpj.substring(size);
    let sum = 0;
    let pos = size - 7;
    for (let i = size; i >= 1; i--)
    {
        sum += numbers.charAt(size - i) * pos--;
        if (pos < 2)
            pos = 9;
    }
    let resultado = sum % 11 < 2 ? 0 : 11 - sum % 11;
    if (resultado != digits.charAt(0))
        return false;

    size = size + 1;
    numbers = cnpj.substring(0, size);
    sum = 0;
    pos = size - 7;
    for (let i = size; i >= 1; i--)
    {
        sum += numbers.charAt(size - i) * pos--;
        if (pos < 2)
            pos = 9;
    }
    resultado = sum % 11 < 2 ? 0 : 11 - sum % 11;
    if (resultado != digits.charAt(1))
        return false;

    return true;

}